# @formbricks/react

## 0.0.3

### Patch Changes

- 933be01: add simple validation, add minLength & maxLength to Text & Textarea
